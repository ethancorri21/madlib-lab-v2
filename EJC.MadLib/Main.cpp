
// MadLib
// Your Name

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

//Prototype
void MadLibPrint(string MadLib[]);

int main()
{
	
	//St. Patricks Themed Mad Lib -- https://www.woojr.com/st-patricks-day-ad-libs-for-kids/st-patricks-wear-green/

	string MadLib[16]; //Starts at zero otherwise would be 16 total.

	cout << "Enter a Month: ";
	getline(cin, MadLib[0]);
	cout << "Enter a verb (-ing): ";
	getline(cin, MadLib[1]);
	cout << "Enter a color: ";
	getline(cin, MadLib[2]);
	cout << "Enter a adjective: ";
	getline(cin, MadLib[3]);
	cout << "Enter a noun: ";
	getline(cin, MadLib[4]);
	cout << "Enter a color: ";
	getline(cin, MadLib[5]);
	cout << "Enter a adjective: ";
	getline(cin, MadLib[6]);
	cout << "Enter a adverb: ";
	getline(cin, MadLib[7]);
	cout << "Enter a plural noun: ";
	getline(cin, MadLib[8]);
	cout << "Enter a article of clothing: ";
	getline(cin, MadLib[9]);
	cout << "Enter a verb: ";
	getline(cin, MadLib[10]);
	cout << "Enter a color: ";
	getline(cin, MadLib[11]);
	cout << "Enter a adjective: ";
	getline(cin, MadLib[12]);
	cout << "Enter a number: ";
	getline(cin, MadLib[13]);
	cout << "Enter a noun: ";
	getline(cin, MadLib[14]);
	cout << "Enter a adjective: ";
	getline(cin, MadLib[15]);

	//Array is not being passed to the method // Lookup how to pass array through including all 
	//for (int i = 0; i <= 16; i++)
	//{	
	//	MadLibPrint(MadLib[i]);
	//}
	MadLibPrint(MadLib);

	//sending to a text file.

	string path = "madlib.txt";

	string y;

	cout << "Would you like to save output to file? " << "(y/n):"; cin >> y;


	if (y == "y" || y == "Y")
	{
		cout << "Mad Lib is being saved to madlib.txt.\n";
		string line;
		ifstream ifs(path);
		while (getline(ifs, line))
		{
			cout << "Every year in " << MadLib[0] << ", people celebrate Irish heritage by ";

			cout << MadLib[1] << " the color " << MadLib[2] << ". ";

			cout << "Some people wear " << MadLib[3] << " hats with a " << MadLib[4];

			cout << " on them. Others wear " << MadLib[5] << " " << MadLib[6] << " ties, or ";

			cout << MadLib[7] << " colored " << MadLib[8] << " " << MadLib[9] << "." << " and pins that say \"" << MadLib[10];

			cout << " me, Im Irish!\" " << "are popular. The color " << MadLib[11] << " is associated with St.Patricks Day partly because Irelands nickname is The " << MadLib[12] << " Isle, and also because of the \"";

			cout << MadLib[13] << " leaf " << MadLib[14] << ", a symbol of " << MadLib[15] << " luck.\n";
		}

		ifs.close();
	}
	


	(void)_getch();
	return 0;
}


void MadLibPrint(string MadLib[])
{
	//Print the array with the Madlib message here // ex: cout << MadLib[1];

	
	
	cout << "Every year in " << MadLib[0] << ", people celebrate Irish heritage by ";

	cout << MadLib[1] << " the color " << MadLib[2] << ". ";

	cout << "Some people wear " << MadLib[3] << " hats with a " << MadLib[4];

	cout << " on them. Others wear " << MadLib[5] << " " << MadLib[6] << " ties, or ";

	cout << MadLib[7] << " colored " << MadLib[8] << " " << MadLib[9] << "." << " and pins that say \"" << MadLib[10];

	cout << " me, Im Irish!\" " << "are popular. The color " << MadLib[11] << " is associated with St.Patricks Day partly because Irelands nickname is The " << MadLib[12] << " Isle, and also because of the \"";

	cout << MadLib[13] << " leaf " << MadLib[14] << ", a symbol of " << MadLib[15] << " luck.\n";

	

}